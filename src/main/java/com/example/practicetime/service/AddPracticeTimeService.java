package com.example.practicetime.service;

import com.example.practicetime.db.PracticeTime;
import com.example.practicetime.db.PracticeTimeRepository;
import com.example.practicetime.dto.RequestDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AddPracticeTimeService {

    private Logger log = LoggerFactory.getLogger(AddPracticeTimeService.class);

    @Autowired
    PracticeTimeRepository repo;

    public void setPracticeTime(RequestDTO request) {
        Calendar start = setTime(
                request.getStart().getYear(),
                request.getStart().getMonth(),
                request.getStart().getDay(),
                request.getStart().getHour(),
                request.getStart().getMin()
                );
        Calendar end = setTime(
                request.getEnd().getYear(),
                request.getEnd().getMonth(),
                request.getEnd().getDay(),
                request.getEnd().getHour(),
                request.getEnd().getMin()
                );

        PracticeTime practiceTime = new PracticeTime();
        practiceTime.setStartTimeMillis((start.getTimeInMillis()/1000)*1000);
        practiceTime.setEndTimeMillis((end.getTimeInMillis()/1000)*1000);
        practiceTime.setTeamID(request.getTeamID());
        repo.save(practiceTime);
        log.info("Practice added Successfully");
    }

    Calendar setTime(int y, int m, int d, int h, int min) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(y, m - 1, d, h, min, 0);
        return calendar;
    }



}
