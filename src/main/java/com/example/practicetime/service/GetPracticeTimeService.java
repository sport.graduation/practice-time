package com.example.practicetime.service;

import com.example.practicetime.db.PracticeTime;
import com.example.practicetime.db.PracticeTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetPracticeTimeService {

    @Autowired
    PracticeTimeRepository repo;

    public PracticeTime getPracticeTimeByTeamID(long teamID) {
        return repo.findTopByTeamIDOrderByIdDesc(teamID);
    }

    public List<PracticeTime> getAllPracticesForTeam(long teamID){
        return repo.findAllByTeamID(teamID);
    }

    public List<PracticeTime> getLast10PracticesForTeam(long teamID){
        return repo.findTop10ByTeamIDOrderByIdDesc(teamID);
    }
}
