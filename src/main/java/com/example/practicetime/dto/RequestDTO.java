package com.example.practicetime.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RequestDTO implements Serializable {
    private long teamID;
    private DateDTO start;
    private DateDTO end;
}
