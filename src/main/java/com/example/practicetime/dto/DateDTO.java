package com.example.practicetime.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DateDTO implements Serializable {
    private int year;
    private int month;
    private int day;
    private int hour;
    private int min;

}
