package com.example.practicetime.controller;

import com.example.practicetime.db.PracticeTime;
import com.example.practicetime.dto.RequestDTO;
import com.example.practicetime.service.AddPracticeTimeService;
import com.example.practicetime.service.GetPracticeTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/practice")
public class PracticeTimeController {


    @Autowired
    AddPracticeTimeService addPracticeTimeService;

    @Autowired
    GetPracticeTimeService getPracticeTimeService;

    @PostMapping(value = "/add")
    public String addPractice(@RequestBody RequestDTO request) {
        addPracticeTimeService.setPracticeTime(request);
        return "Practice has been added";
    }

    @GetMapping(value = "/get/{id}")
    public ResponseEntity<PracticeTime> getPracticeTime(@PathVariable("id") long teamID){
        PracticeTime practiceTime = getPracticeTimeService.getPracticeTimeByTeamID(teamID);
        return ResponseEntity.status(HttpStatus.OK).body(practiceTime);
    }



    @GetMapping(value = "/last/{id}")
    public ResponseEntity<List<PracticeTime>> getLast10PracticesForTeam(@PathVariable("id") long teamID){
        List<PracticeTime> practiceTime = getPracticeTimeService.getLast10PracticesForTeam(teamID);
        return ResponseEntity.status(HttpStatus.OK).body(practiceTime);
    }

    @GetMapping(value = "/hi")
    public String hi(){
        return "hi";
    }

    /*@GetMapping(value = "all/{id}")
    public ResponseEntity<List<PracticeTime>> getAllPracticeTime(@PathVariable("id") long teamID){
        List<PracticeTime> practiceTime = getPracticeTimeService.getAllPracticesForTeam(teamID);
        return ResponseEntity.status(HttpStatus.OK).body(practiceTime);
    }*/
}