package com.example.practicetime.db;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PracticeTimeRepository extends JpaRepository<PracticeTime, Long> {
    PracticeTime findTopByTeamIDOrderByIdDesc(long teamID);
    List<PracticeTime> findTop10ByTeamIDOrderByIdDesc(long teamID);
    List<PracticeTime> findAllByTeamID(long teamID);
}
