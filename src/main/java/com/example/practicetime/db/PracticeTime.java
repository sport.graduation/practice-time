package com.example.practicetime.db;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "practiceTime")
@Entity
public class PracticeTime implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "practiceTime")
    @SequenceGenerator(name="practiceTime", sequenceName = "practiceTime_seq")
    private long id;
    private long teamID;
    private long startTimeMillis;
    private long endTimeMillis;

}
