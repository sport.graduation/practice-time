FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 30110

COPY target/*.jar practicetime.jar

ENTRYPOINT [ "java","-jar","./practicetime.jar"]